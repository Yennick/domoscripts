#!/bin/bash
#
# simple script to send email alert for ip changes
# version 0.1
# It needs 3 files, 'log.txt', 'ip.log' and 'pushover.sh'

# If you want a simple log file, assign 1 to log_enabled, otherwise set it to 0
log_enabled=0; #0=disable, 1=enable

# date format used by log file
datestamp=`date '+%Y-%m-%d %H:%M:%S'`

# the actual command getting the public IP, change the
# URL to your php hosting if you have one
myipnow=`wget -4 -qO - icanhazip.com` 

previp="0.0.0.0";

# path of the log file, ignore if log_enabled=0
logfile="/home/domoticz/domoticz/scripts/bash/ipcheck/log.txt" #create this file yourself

# path of the temporary file storing previous ip address
iplog="/home/domoticz/domoticz/scripts/bash/ipcheck/ip.log" #create this file yourself

if [ -f $iplog ]; then
   previp=`cat $iplog`
fi

if [ $myipnow != $previp ]; then
   #ip changed, sending alert
   /bin/bash /home/domoticz/domoticz/scripts/bash/pushover.sh -u YOURUSERKEY -a YOURAPPLICATIONKEY -q "IP-adres gewijzigd" -m "Was $previp is nu $myipnow"


   #write the new ip to log file
   echo $myipnow > $iplog
   if [ $log_enabled = 1 ]; then
      echo "$datestamp IP changed, sending notification email. $previp | 
      $myipnow" >> $logfile
   fi
else
   if [ $log_enabled = 1 ]; then
      echo "$datestamp IP is same, skipping notification. $previp | 
      $myipnow" >> $logfile
   fi
fi

